ARG POSTGRES_VERSION

FROM postgres:${POSTGRES_VERSION}-alpine

ENV POSTGRES_VERSION=${POSTGRES_VERSION}
ENV POSTGRES_USER='postgres'
ENV BACKUP_USER=${POSTGRES_USER}
ENV BACKUP_SQL_USER=${POSTGRES_USER}
ENV BACKUP_DIR='/home/backups/database/postgresql'
ENV BACKUP_HOSTNAME=''
ENV BACKUP_SCHEMA_ONLY_LIST=''
ENV BACKUP_CUSTOM_BACKUPS='yes'
ENV BACKUP_PLAIN_BACKUPS='yes'
ENV BACKUP_ENABLE_GLOBALS_BACKUP='yes'
ENV ROTATED_DAY_OF_WEEK_TO_KEEP='5'
ENV ROTATED_DAYS_TO_KEEP='7'
ENV ROTATED_WEEKS_TO_KEEP='5'

COPY --from=hairyhenderson/gomplate:stable /gomplate /usr/local/bin/gomplate
COPY docker-entrypoint.sh /usr/local/bin/
COPY pg_backup /usr/local/bin/
COPY pg_backup_rotated /usr/local/bin/
COPY pg_backup.config.tmpl /var/lib/postgresql/
COPY s3cfg.tmpl /var/lib/postgresql/

COPY generate-configs.sh /docker-entrypoint.d/

RUN apk update && apk add python3 py3-pip zip && \
    pip install s3cmd && \
    mkdir -p /home/backups/database && \
    chown -R postgres:postgres /home/backups/

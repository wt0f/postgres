[![pipeline status](https://gitlab.com/wt0f/postgres/badges/main/pipeline.svg)](https://gitlab.com/wt0f/postgres/-/commits/main)
[![Latest Release](https://gitlab.com/wt0f/postgres/-/badges/release.svg)](https://gitlab.com/wt0f/postgres/-/releases)

PostgreSQL Container
====================

Standard Bitnami PostgreSQL container with additional scripts included.
This container is based on Alpine Linux.

PostgreSQL Variables
--------------------

The following variables are used to configure the PostgreSQL container.

| Name                  | Default Value | Notes                       |
|-----------------------|---------------|-----------------------------|
| POSTGRES_PASSWORD     |               |                             |
| POSTGRES_PASSWORD_FILE |              |                             |
| POSTGRES_USER         |  `postgres`   |                             |
| POSTGRES_USER_FILE    |               |                             |
| POSTGRES_DB           | `$POSTGRES_USER` |                          |
| POSTGRES_DB_FILE      |               |                             |
| POSTGRES_INITDB_ARGS  |               |                             |
| POSTGRES_INITDB_ARGS_FILE |           |                             |
| POSTGRES_INITDB_WALDIR |              |                             |
| POSTGRES_HOST_AUTH_METHOD | `scram-sha-256` |                       |
| PGDATA                | `/var/lib/postgresql/data` |                |

Backup Variables
----------------

The following variables are used to configure the backup script used to
perform backups of the PostgreSQL databases.

| Name                  | Default Value | Notes                       |
|-----------------------|---------------|-----------------------------|
| BACKUP_USER           | `$POSTGRES_USER` |                          |
| BACKUP_HOSTNAME       |               |                             |
| BACKUP_SQL_USER       | `$POSTGRES_USER` |                          |
| BACKUP_DIR            | `/home/backups/database/postgresql` |       |
| BACKUP_SCHEMA_ONLY_LIST |             |                             |
| BACKUP_CUSTOM_BACKUPS |     `yes`     |                             |
| BACKUP_PLAIN_BACKUPS  |     `yes`     |                             |
| BACKUP_ENABLE_GLOBALS_BACKUP | `yes`  |                             |
| ROTATED_DAY_OF_WEEK_TO_KEEP |  `7`    |                             |
| ROTATED_DAYS_TO_KEEP  |     `5`       |                             |
| ROTATED_WEEKS_TO_KEEP |     `7`       |                             |

Backup Transfer Variables
-------------------------

The following variables are used to configure `s3cmd` to transfer backups
to an S3 compatible object store.

| Name                  | Default Value | Notes                       |
|-----------------------|---------------|-----------------------------|
| S3_ACCESS_KEY         |               |                             |
| S3_SECRET_KEY         |               |                             |
| S3_BUCKET_LOCATION    |               |                             |
| S3_HOST_BASE          |               |                             |
| S3_HOST_BUCKET        |               |                             |
| S3_GUESS_MIME_TYPE    |               |                             |
| S3_USE_HTTPS          |               |                             |
| S3_EXPIRY_DAYS        |               |                             |

